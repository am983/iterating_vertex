#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 11:59:01 2022

@author: ali
"""

pip install xarray

import numpy as np

data_path='/Users/ali/Desktop/Jul/apoe/python_vertex_screen/' 
x=np.load(data_path +"connectivity.npy")
x.shape
xx=x.transpose()
xx.shape
x=xx


####age
import pyreadr

y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Age_Months"]








########################### diet
y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Diet"]
y[y=="HFD"]=-1;
y[y=="Control"]=1;
type(y)
y


########################### geno
y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Genotype"]
print(np.unique(y))
y[y=="APOE22"]=1;
y[y=="APOE22HN"]=1;
y[y=="APOE33"]=2;
y[y=="APOE33HN"]=2;
y[y=="APOE44"]=3;
y[y=="APOE44HN"]=3;

y

########################### sex
y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Sex"]
y[y=="male"]=-1;
y[y=="female"]=1;
type(y)
y

########################### geno,diet 
#########################
####################
############3
y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Genotype"]
print(np.unique(y))
y[y=="APOE22"]=1;
y[y=="APOE22HN"]=1;
y[y=="APOE33"]=2;
y[y=="APOE33HN"]=2;
y[y=="APOE44"]=3;
y[y=="APOE44HN"]=3;

y1=y



y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Diet"]
y[y=="HFD"]=-1;
y[y=="Control"]=1;
type(y)
y2=y


y=np.column_stack((y1,y2))





########################### geno,diet , age, sex
#########################
####################
############3
y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Genotype"]
print(np.unique(y))
y[y=="APOE22"]=1;
y[y=="APOE22HN"]=1;
y[y=="APOE33"]=2;
y[y=="APOE33HN"]=2;
y[y=="APOE44"]=3;
y[y=="APOE44HN"]=3;

y1=y



y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Diet"]
y[y=="HFD"]=-1;
y[y=="Control"]=1;
type(y)
y2=y


y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Age_Months"]
y3=y


y=pyreadr.read_r(data_path + 'response.rda')
y=y["response"]
print(y)
y=y["Sex"]
y[y=="male"]=-1;
y[y=="female"]=1;
type(y)
y4=y


#y=np.column_stack((y1,y2, y3, y4))
#y=np.column_stack((y1,y2, y4))
y=np.column_stack((y1,y2))
#y=np.column_stack((y1,y4))
